import unittest
from transactions import Account
from decimal import Decimal


class TransactionTests(unittest.TestCase):

    def test_dispute(self):
        acct = Account()
        acct.deposit(1, Decimal("100"))
        acct.dispute(1)
        self.assertEqual(acct.get_available(), 0)
        self.assertEqual(acct.total, 100)
        self.assertEqual(acct.held, 100)

    def test_deposit_chargeback(self):
        acct = Account()
        acct.deposit(1, Decimal("100"))
        acct.dispute(1)
        acct.chargeback(1)
        self.assertTrue(acct.is_locked)
        self.assertEqual(acct.total, 0)
        self.assertEqual(acct.held, 0)

    def test_withdraw_chargeback(self):
        acct = Account()
        acct.deposit(1, Decimal("100"))
        acct.withdraw(2, Decimal("50"))
        acct.dispute(2)
        acct.chargeback(2)
        self.assertTrue(acct.is_locked)
        self.assertEqual(acct.total, 100)
        self.assertEqual(acct.held, 0)

    def test_deposit_resolve(self):
        acct = Account()
        acct.deposit(1, Decimal("100"))
        acct.dispute(1)
        acct.resolve(1)
        self.assertFalse(acct.is_locked)
        self.assertEqual(acct.total, 100)
        self.assertEqual(acct.held, 0)

    def test_withdraw_resolve(self):
        acct = Account()
        acct.deposit(1, Decimal("100"))
        acct.withdraw(2, Decimal("50"))
        acct.dispute(2)
        acct.resolve(2)
        self.assertFalse(acct.is_locked)
        self.assertEqual(acct.total, 50)
        self.assertEqual(acct.held, 0)


if __name__ == '__main__':
    unittest.main()
