from decimal import Decimal
from collections import defaultdict
import sys


class FailedTransactionException(Exception):
    def __init__(self, message):
        self.message = message


class TransactionState:
    normal = 0
    disputed = 1
    reversed = 2


class DepositOrWithdrawalTransaction:
    def __init__(self, account_change):
        self.state = TransactionState.normal
        self.account_change = account_change

    def set_to_disputed(self):
        if self.state != TransactionState.normal:
            raise FailedTransactionException('Cannot dispute: state is %s' % self.state)
        self.state = TransactionState.disputed
    
    def set_to_resolved(self):
        if self.state != TransactionState.disputed:
            raise FailedTransactionException('Cannot resolve: state is %s' % self.state)
        self.state = TransactionState.normal
        
    def set_to_reversed(self):
        if self.state != TransactionState.disputed:
            raise FailedTransactionException('Cannot chargeback: state is %s' % self.state)
        self.state = TransactionState.reversed


class Account:
    def __init__(self):
        self.total = Decimal('0')
        self.held = Decimal('0')
        self.transactions = {}
        self.is_locked = False

    def ensure_not_locked(self):
        if self.is_locked:
            raise FailedTransactionException('Account is locked')

    def ensure_not_duplicate(self, transaction_id):
        if transaction_id in self.transactions:
            raise FailedTransactionException('Transaction already exists')

    def ensure_transaction_exists(self, transaction_id):
        if transaction_id not in self.transactions:
            raise FailedTransactionException('Transaction does not exist')

    def get_available(self):
        return self.total - self.held
        
    def withdraw(self, transaction_id, amount):
        self.ensure_not_locked()
        self.ensure_not_duplicate(transaction_id)
        if self.get_available() < amount:
            raise FailedTransactionException('Not enough money to withdraw')
        self.transactions[transaction_id] = DepositOrWithdrawalTransaction(-amount)
        self.total -= amount
        
    def deposit(self, transaction_id, amount):
        self.ensure_not_locked()
        self.ensure_not_duplicate(transaction_id)
        self.transactions[transaction_id] = DepositOrWithdrawalTransaction(amount)
        self.total += amount
        
    def dispute(self, transaction_id):
        self.ensure_not_locked()
        self.ensure_transaction_exists(transaction_id)
        transaction = self.transactions[transaction_id]
        transaction.set_to_disputed()
        self.held += transaction.account_change
            
    def resolve(self, transaction_id):
        self.ensure_not_locked()
        self.ensure_transaction_exists(transaction_id)
        transaction = self.transactions[transaction_id]
        transaction.set_to_resolved()
        self.held -= transaction.account_change
            
    def chargeback(self, transaction_id):
        self.ensure_not_locked()
        self.ensure_transaction_exists(transaction_id)
        transaction = self.transactions[transaction_id]
        transaction.set_to_reversed()
        self.held -= transaction.account_change
        self.total -= transaction.account_change
        self.is_locked = True


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: %s input.csv" % sys.argv[0], file=sys.stderr)
        sys.exit(1)
    
    filename = sys.argv[1]
    accounts = defaultdict(Account)
    
    for line_number, line in enumerate(open(filename)):
        fields = line.split(',')
        transact_type = fields[0].strip()
        
        if transact_type != 'type':  # ignore the title row
            client_id = int(fields[1])
            tx_id = int(fields[2])
            tx_amount = Decimal(fields[3]) if len(fields) > 3 else None
            try:
                if transact_type == 'deposit':
                    accounts[client_id].deposit(tx_id, tx_amount)
                elif transact_type in ('withdraw', 'withdrawal'):
                    accounts[client_id].withdraw(tx_id, tx_amount)
                elif transact_type == 'dispute':
                    accounts[client_id].dispute(tx_id)
                elif transact_type == 'resolve':
                    accounts[client_id].resolve(tx_id)
                elif transact_type == 'chargeback':
                    accounts[client_id].chargeback(tx_id)
                else:
                    print('Unexpected transaction type %s' % transact_type, file=sys.stderr)
            except FailedTransactionException as e:
                print('%s record #%s: Transaction %s for client %s failed: %s' %
                      (filename, line_number, tx_id, client_id, e.message), file=sys.stderr)
    print('client', 'available', 'held', 'total', 'locked', sep=', ')
    for client_id, account in accounts.items():
        print(client_id, account.get_available(), account.held, account.total, str(account.is_locked).lower(), sep=', ')
        