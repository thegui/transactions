# How to run

To run program:
`python3 transactions.py input.csv > output.csv`

To run tests:
`python3 transaction_tests.py`

Notes:
* For any failed transactions, a warning message is printed to stderr
* This project does not depend on any external libaries.
* It should work with version 3.\* of python
* It was specifically tested using python version 3.8.5

# Test data

Some sample test data is included as `test_data/test[12].csv`, and the corresponding expected output is `test_data/test[12].expected.csv`. I used these tests for manual verification
